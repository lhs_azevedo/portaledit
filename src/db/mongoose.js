const mongoose = require('mongoose');
const logger = require('../util/logger.js');

const { MONGODB_URI } = process.env;

logger.info('Connecting');
mongoose.connect(MONGODB_URI, {
  useNewUrlParser: true,
});
const db = mongoose.connection;

db.on('error', console.error.bind(console, 'connection error:'));

module.exports = db;
