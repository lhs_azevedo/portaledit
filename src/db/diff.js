const mongoose = require('mongoose');

const diffSchema = new mongoose.Schema({
  titleDelta: { type: String, required: true },
  descriptionDelta: { type: String, required: true },
  bodyDelta: { type: String, required: true },
  type: { type: String },
  differ: { type: String },
  created: { type: Date, default: Date.now },
});

module.exports = mongoose.model('Diff', diffSchema);
