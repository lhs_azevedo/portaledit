const mongoose = require('mongoose');
const Diff = require('./diff');

const articleSchema = new mongoose.Schema({
  feedId: { type: Number, required: true, index: true },
  feedName: { type: String, required: true },
  portal: { type: String, required: true },
  link: {
    type: String, index: true, unique: true, required: true,
  },
  title: { type: String, required: true },
  description: { type: String, default: '' },
  body: { type: String, required: true },
  author: String,
  diffs: [Diff.schema],
  diffsCount: { type: Number, index: true },
  createdAt: { type: Date, default: Date.now },
  lastCheckedAt: { type: Date, default: Date.now },
  lastEditedAt: { type: Date }
});

articleSchema.index({ title: 'text' }, { default_language: 'portuguese' });

articleSchema.methods.isStale = function isStale() {
  const hotness = (Date.now() - this.createdAt) / 1000;
  const staleness = (Date.now() - this.lastCheckedAt) / 1000;
  const ratio = staleness / hotness;

  if (ratio > 0.2) {
    return true;
  }
  return false;
};

articleSchema.pre('save', function updateDiffCount(next) {
  this.diffsCount = this.diffs.length;
  if (this.diffsCount > 0) {
    let diffs = this.diffs.toObject();
    diffs.sort((a,b) => b.created - a.created);
    this.lastEditedAt = diffs[0].created;
  } 
  next();
});

module.exports = mongoose.model('Article', articleSchema);
