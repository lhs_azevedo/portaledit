const createPatch = require('textdiff-create');
const applyPatch = require('textdiff-patch');

const delta = function (v1, v2) {
  return createPatch(v1, v2);
};

const getCurrentState = function (article) {
  if (article.diffsCount === 0) return article;
  const reducer = (current, diff) => {
    current.title = applyPatch(current.title, JSON.parse(diff.titleDelta));
    current.description = applyPatch(current.description, JSON.parse(diff.descriptionDelta));
    current.body = applyPatch(current.body, JSON.parse(diff.bodyDelta));
    return current;
  };
  const initial = {
    title: article.title,
    description: article.description,
    body: article.body,
  };

  return article.diffs.reduce(reducer, initial);
};

module.exports = {
  delta,
  getCurrentState,
};
