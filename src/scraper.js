const cheerio = require('cheerio');
const scrapeIt = require('scrape-it');

const scrapeArticle = (body, feedInfo) => {
  const $ = cheerio.load(body);
  if ($('meta[property="og:type"]').attr('content') !== 'article') {
    return false;
  }
  feedInfo.htmlPreProcess($);
  return scrapeIt.scrapeHTML($, feedInfo.scrapeOptions);
};

module.exports = {
  scrapeArticle,
};
