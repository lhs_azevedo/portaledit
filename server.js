const express = require('express');
require('dotenv').load();

const app = express();

const marked = require('marked');
const diff = require('./src/lib/htmldiff/htmldiff');
const db = require('./src/db/mongoose');
const Article = require('./src/db/article');
const differ = require('./src/differ');
const moment = require('moment');
moment.locale('pt-br');
moment.updateLocale('pt-br', {
  relativeTime : {
      // future: "in %s",
      past:   "%s",
      s  : 's',
      ss : '%ds',
      m:  "1min",
      mm: "%dmin",
      h:  "1h",
      hh: "%dh",
      d:  "1d",
      dd: "%dd",
      M:  "1m",
      MM: "%dm",
      y:  "1a",
      yy: "%da"
  }
});

app.set('view engine', 'pug');

app.get('/', async (req, res) => {
  const articles = [];
  articles.mostEdited = await Article.find({ diffsCount: { $gte: 1 } }).sort({diffsCount: 'desc'}).limit(10);
  articles.latestEdit = await Article.find({ diffsCount: { $gte: 1 } }).sort({lastEditedAt: 'desc'}).limit(10);
  res.render('index', { articles: articles, moment: moment, greyBody: true });
});

app.get('/article/:id', async (req, res) => {
  const article = await Article.findOne({ _id: req.params.id });
  article.parsedBody = marked(article.body);

  const currentArticle = differ.getCurrentState(article);
  currentArticle.parsedBody = marked(currentArticle.body);

  const renderedDiff = {
    title: diff(article.title, currentArticle.title),
    description: diff(article.description, currentArticle.description),
    body: diff(article.parsedBody, currentArticle.parsedBody)
  };


  res.render('article', {
    originalArticle: article,
    renderedDiff,
  });
});

app.get('/pesquisar', async (req, res) => {
  const articles = await Article.find({ $text: { $search: req.query.titulo }, diffsCount: { $gt: 0 } }).sort({ diffsCount: 'desc' });

  res.render('search', { articles, greyBody: true, moment });
});

app.listen(process.env.PORT, () => console.log('Listening on port 8080'));
