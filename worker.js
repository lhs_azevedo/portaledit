require('dotenv').load();
const Parser = require('rss-parser');
const got = require('got');
const cheerio = require('cheerio');
const scrapeIt = require('scrape-it');
const fs = require('fs');
const db = require('./src/db/mongoose');
const logger = require('./src/util/logger');
const scraper = require('./src/scraper');
const differ = require('./src/differ');

const Article = require('./src/db/article');
const Diff = require('./src/db/diff');

const feedsInfo = require('./feeds.js');

db.once('open', () => {
  logger.info('Database connected');
  parseFeeds()
    .then(checkStaleArticles)
    .then(() => process.exit());
});

parseFeeds = async () => {
  logger.info('Parsing feeds...');
  parser = new Parser();

  parsingFeeds = feedsInfo.map(feedInfo => parseFeed(feedInfo)
    .then(processFeedItems)
    .catch((reason) => {
      logger.error(reason);
      process.exit(1);
    }));

  return Promise.all(parsingFeeds).then((processedFeeds) => {
    const totalNewArticles = processedFeeds.reduce((feedsAcc, processedFeedData) => {
      const newArticles = processedFeedData.processedFeed.reduce((feedAcc, article) => ((article !== false) ? feedAcc + 1 : feedAcc), 0);
      feedsAcc.total += newArticles;
      feedsAcc.feeds.push({ name: processedFeedData.feedInfo.name, total: newArticles });
      return feedsAcc;
    }, { total: 0, feeds: [] });
    logger.info(`Done parsing, ${totalNewArticles.total} new articles. (${totalNewArticles.feeds.map(i => `${i.name}: ${i.total}`).join(', ')})`);
  });
};

parseFeed = async (feedInfo) => {
  logger.info(`Parsing ${feedInfo.name}`);
  return parser.parseURL(feedInfo.link)
    .then(parsedFeed => ({
      feedInfo,
      parsedFeed,
    }));
};

processFeedItems = async (feedData) => {
  processedFeedItems = await Promise.all(feedData.parsedFeed.items.map(parsedFeedItem => processFeedItem(parsedFeedItem, feedData.feedInfo)));
  return {
    ...feedData,
    processedFeed: processedFeedItems,
  };
};

checkIfParsedFeedItemIsNew = async parsedFeedItem => Article.findOne({ link: parsedFeedItem.link }).exec().then((article) => {
  if (article) return false;
  return true;
});

processFeedItem = async (parsedFeedItem, feedInfo) => {
  logger.debug('Parsing feed item', { link: parsedFeedItem.link });
  const isNew = await checkIfParsedFeedItemIsNew(parsedFeedItem);
  if (!isNew) return false;
  const articleResponse = await got(parsedFeedItem.link);
  const scrapedData = scraper.scrapeArticle(articleResponse.body, feedInfo);
  if (!scrapedData) {
    return false;
  }
  const article = new Article({
    feedId: feedInfo.id,
    feedName: feedInfo.name,
    portal: feedInfo.portal,
    link: parsedFeedItem.link,
    title: scrapedData.title,
    description: scrapedData.description,
    body: scrapedData.body,
    author: scrapedData.author,
  });
  return article.save().then((article) => {
    logger.debug(`Article saved: ${article.title.slice(0, 40)}...`);
    return article;
  });
};

checkStaleArticles = async () => {
  logger.info('Collecting stale articles...');
  return Article.find({}).exec()
    .then(articles => Promise.all(articles.map(checkStaleArticle)))
    .then(articles => logger.info(`Done checking, ${articles.reduce((acc, item) => ((item !== false) ? acc + 1 : acc), 0)} edits`));
};

checkStaleArticle = async (article) => {
  if (!article.isStale()) {
    return false;
  }

  const currentState = differ.getCurrentState(article);

  logger.debug(`Checking article for edits: ${article.title.slice(0, 40)}...`);
  const articleResponse = await got(article.link);
  const scrapedData = scraper.scrapeArticle(articleResponse.body, feedsInfo.find(feed => feed.id === article.feedId));

  if (
    currentState.title === scrapedData.title
        && currentState.description === scrapedData.description
        && currentState.body === scrapedData.body
  ) {
    return false;
  }

  logger.debug(`Edit found for: ${article.title.slice(0, 40)}...`);

  const diff = {
    titleDelta: differ.delta(currentState.title, scrapedData.title),
    descriptionDelta: differ.delta(currentState.description, scrapedData.description),
    bodyDelta: differ.delta(currentState.body, scrapedData.body),
  };


  diff.titleDelta = JSON.stringify(diff.titleDelta);
  diff.descriptionDelta = JSON.stringify(diff.descriptionDelta);
  diff.bodyDelta = JSON.stringify(diff.bodyDelta);
  diff.version = 'v1';

  logger.debug(`Will save: ${article.title.slice(0, 40)}...`);
  article.lastCheckedAt = Date.now();
  article.diffs.push(diff);
  return article.save();
};
