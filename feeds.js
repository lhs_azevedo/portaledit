const TurndownService = require('turndown');

const turndownService = new TurndownService();
const cheerio = require('cheerio');

module.exports = [
  {
    id: 1,
    name: 'G1 Política',
    portal: 'G1',
    link: 'http://pox.globo.com/rss/g1/politica/',
    htmlPreProcess: ($) => {
      $('article meta,.content-media.content-video').remove();
      $('.content-media.content-photo').map((i, elem) => {
        const $elem = $(elem);
        const url = $elem.find('.progressive-img').data('max-size-url');
        const alt = $elem.find('.content-media__description').text();
        const $imgDoc = cheerio.load('<img>');
        $(elem).replaceWith($imgDoc('img').attr('src', url).attr('alt', alt).attr('title', alt));
      });
      $('.content-media').remove();
    },
    scrapeOptions: {
      title: 'main h1.content-head__title',
      description: 'main h2.content-head__subtitle',
      author: {
        selector: 'main p.content-publication-data__from',
        attr: 'title',
        convert: (author) => {
          if (author.startsWith('Por ')) {
            return author.slice(4);
          }
          return author;
        },
      },
      body: {
        selector: 'article',
        convert: content => turndownService.turndown(content),
        how: 'html',
      },
    },
  },
];
